import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';

import { AccordionListComponent } from './accordion-list';

@NgModule({
	declarations: [
    AccordionListComponent
  ],
	imports: [
    IonicModule
  ],
	exports: [
    AccordionListComponent
  ]
})
export class AccordionListModule {}
