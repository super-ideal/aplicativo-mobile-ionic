import { Injectable } from '@angular/core';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/retry';

import { ApiService } from '../api';

@Injectable()
export class AuthProvider {

  constructor(public api: ApiService) {
  }

  public getName(email_cpfcnpj) {
    return new Promise((resolve, reject) => {
      let seq = this.api.get('auth/name/', email_cpfcnpj).share();

      seq
        .timeout(10000)
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }

  public getUser() {
    return new Promise((resolve, reject) => {
      let seq = this.api.get('auth/me').share();

      seq
        .timeout(10000)
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }

  public login(body) {
    return new Promise((resolve, reject) => {
      let seq = this.api.post('auth/login', body).share();

      seq
        .timeout(10000)
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }

  public register(body) {
    return new Promise((resolve, reject) => {
      let seq = this.api.post('auth/register', body).share();

      seq
        .timeout(20000)
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }
}
