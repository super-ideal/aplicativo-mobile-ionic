import { NgModule } from '@angular/core';

import { ApiService } from './api';
import { AppConfig } from './app.config';
import { AuthProvider } from './auth/auth';
import { LocalStorageProvider } from './local-storage/local-storage';

import { ValidadorCpfCNPJ } from './shared/validator-cpf-cnpj/validador';
import { FlashProvider } from './shared/flash/flash';
import { ConnectivityProvider } from './connectivity/connectivity';
import { GoogleMapsProvider } from './google-maps/google-maps';
import { OffersProvider } from './offers/offers';
import { ProductsProvider } from './products/products';
import { SectionsProvider } from './sections/sections';
import { ShoppingListProvider } from './shopping-list/shopping-list';
import { ShoppingListProductsProvider } from './shopping-list/products-shopping-list';

@NgModule({
  imports: [
  ],
  providers: [
    ConnectivityProvider,
    LocalStorageProvider,
    FlashProvider,
    ValidadorCpfCNPJ,
    AppConfig,
    ApiService,
    AuthProvider,
    GoogleMapsProvider,
    OffersProvider,
    ProductsProvider,
    SectionsProvider,
    ShoppingListProvider,
    ShoppingListProductsProvider
  ]
})
export class ProviderModule {}
