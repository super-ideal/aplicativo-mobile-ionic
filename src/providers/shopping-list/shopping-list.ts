import { Injectable } from '@angular/core';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/retry';

import { ApiService } from '../api';

@Injectable()
export class ShoppingListProvider {

  constructor(public api: ApiService) {
  }

  public getAllPublic(pagination = true, listProducts = true, page: number = 1, wBy: string = 'id', w: string = '') {
    return new Promise((resolve, reject) => {
      this.api.get('app/shopping/lists/public', {
        'pagination': pagination,
        'listProducts': listProducts,
        'page': page,
        'wBy': wBy,
        'w': w,
        'oBy': 'id',
        'oSort': 'desc'
      })
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }

  public getAll(pagination = true, listProducts = true, page: number = 1, wBy: string = 'id', w: string = '') {
    return new Promise((resolve, reject) => {
      this.api.get('app/shopping/lists', {
        'pagination': pagination,
        'listProducts': listProducts,
        'page': page,
        'wBy': wBy,
        'w': w,
        'oBy': 'id',
        'oSort': 'desc'
      })
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public get(idList: number) {
    return new Promise((resolve, reject) => {
      this.api.get(`app/shopping/lists/${idList}`)
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }

  public post(body) {
    return new Promise((resolve, reject) => {
      this.api.post('app/shopping/lists', body)
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }

  public update(idList: number, body: any) {
    return new Promise((resolve, reject) => {
      this.api.patch(`app/shopping/lists/${idList}`, body)
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }

  public delete(idList: number) {
    return new Promise((resolve, reject) => {
      this.api.delete(`app/shopping/lists/${idList}`)
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }

  public copyList(idList: number) {
    return new Promise((resolve, reject) => {
      this.api.get(`app/shopping/lists/${idList}/copy`)
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }
}
