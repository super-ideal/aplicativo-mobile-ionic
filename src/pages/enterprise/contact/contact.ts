import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { GoogleMapsProvider } from '../../../providers/google-maps/google-maps';

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('pleaseConnect') pleaseConnect: ElementRef;
  protected params: any;

  constructor(private maps: GoogleMapsProvider,) {
    this.params = {
      map_center_coordinates: {
        lat: -18.999873,
        lng: -46.310939
      },
      text_marker: 'Super Ideal',
      zoomMap: 18,
      zoomControlMap: true
    }
  }

  ionViewDidLoad() {
    this.maps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement, this.params);
  }
}
