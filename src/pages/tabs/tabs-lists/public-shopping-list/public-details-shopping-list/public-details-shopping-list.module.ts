import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { CardOffersModule } from '../../../../../components/card-offers/card-offers.module';
import { IonicImageLoader } from 'ionic-image-loader';
import { PublicDetailsShoppingListPage } from './public-details-shopping-list';
import { PipesModule } from '../../../../../pipes/pipes.module';

@NgModule({
  declarations: [
    PublicDetailsShoppingListPage,
  ],
  imports: [
    IonicPageModule.forChild(PublicDetailsShoppingListPage),
    CardOffersModule,
    IonicImageLoader,
    PipesModule
  ]
})
export class PublicDetailsShoppingListModule {}
