import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrivateShoppingListPage } from './private-shopping-list';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

@NgModule({
  declarations: [
    PrivateShoppingListPage,
  ],
  imports: [
    IonicPageModule.forChild(PrivateShoppingListPage),
  ],
  providers: [
    BarcodeScanner
  ]
})
export class PrivateShoppingListModule {}
