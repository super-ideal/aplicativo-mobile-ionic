import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, FabContainer, LoadingController, ModalController, AlertController, PopoverController, ActionSheetController } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { Toast } from '@ionic-native/toast';
import lodash from 'lodash';

import { FlashProvider } from '../../../../../providers/shared/flash/flash';
import { ProductsProvider } from '../../../../../providers/products/products';
import { ShoppingListProductsProvider } from '../../../../../providers/shopping-list/products-shopping-list';
import { ShoppingListProvider } from '../../../../../providers/shopping-list/shopping-list';
import { ImageLoader } from 'ionic-image-loader';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-details-shopping-list',
  templateUrl: 'details-shopping-list.html',
})
export class DetailsShoppingListPage {

  private optionsBarcode: BarcodeScannerOptions;
  loading: any;
  pressed = false;

  list: any;
  products: any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public popoverCtrl: PopoverController,
    public actionSheetCtrl: ActionSheetController,
    public toast: Toast,
    private socialSharing: SocialSharing,
    public flashProvider: FlashProvider,
    public barCode: BarcodeScanner,
    public _shoppingListProvider: ShoppingListProvider,
    public _shoppingListProductsProvider: ShoppingListProductsProvider,
    public _products: ProductsProvider,
    public imageLoader: ImageLoader
  ) {
    this.list = this.navParams.get('data');
    this.imageLoader.clearCache();
  }

  async ionViewDidEnter() {
    this.getProductsList();
  }

  async getProduct(text, whereBy = 'barcode') {
    this.showLoader();

    await this._products.getAll({ whereBy: whereBy, where: text })
      .then(async res => {
        let product_id = res['data']['data'][0]['id'];
        this.postProductList(product_id);
      }, (err) => {
        this.loading.dismiss();
        if (err.status === 404) {
          this.showAlert('Nada encontrado', 'Nenhum produto foi encontrado com o código de barras informado.');
        } else {
          this.flashProvider.show('Erro interno. Tente novamente!', 'error', 3000)
        }
        console.log('erro ', err);
      })
  }

  async postProductList(idProduct) {
    await this._shoppingListProductsProvider.post(idProduct, this.list['id'])
      .then((res) => {
        this.showToast('Produto adicionado a lista de compras');
        this.loading.dismiss();
        this.getProductsList();
      }, (err) => {
        this.loading.dismiss();
        if (err.status === 500 && err.error.code === 'ER_DUP_ENTRY') {
          this.showToast('O código de barras informado já existe na lista');
        } else {
          this.flashProvider.show('Erro interno. Tente novamente!', 'error', 3000)
        }
        console.log('erro ', err);
      });
  }

  getProductsList() {
    this.showLoader();

    this._shoppingListProductsProvider.getAll(this.list['id'])
      .then((res) => {
        this.loading.dismiss();
        this.products = this.imagesProducts(res['data']);
      }, (err) => {
        console.log('erro ', err);

        this.loading.dismiss();
        if (err.status !== 404) {
          this.flashProvider.show('Não foi possível buscar os produtos da lista de compra no servidor.', 'error', 3000)
        }

        // carrega do cache
        this.products = this.imagesProducts(this.list['listProducts']);
      });
  }

  private imagesProducts(products) {
    products.forEach((element, index) => {
      let images = element['product']['images'];
      images.forEach((element, index) => {
        images[index]['url'] = `http://54.233.242.37/image/product/${element['filename']}?format=jpg&quality=80&width=100`
      });
      products[index]['product']['images'] = images;
    });
    return products;
  }

  pressedLong(product, index) {
    if (this.pressed === false) {
      this.pressed = true;
      this.actionSheet(product, index);
    }
  }

  actionSheet(product, index) {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Opções do produto',
      enableBackdropDismiss: true,
      buttons: [
        {
          text: 'Editar',
          icon: 'create',
          handler: () => {
            this.editProduct(product, index);
          }
        }, {
          text: 'Excluir',
          icon: 'trash',
          handler: () => {
            this.deleteProduct(product, index);
          }
        }
      ]
    });
    actionSheet.present();
  }

  editProduct(product, index) {
    const modal = this.modalCtrl.create(
      'FormProductPage',
      { productList: product, idList: this.list['id'] },
      { enableBackdropDismiss: true, showBackdrop: true }
    )
    modal.present();

    modal.onDidDismiss(data => {
      this.getProductsList();
    });
  }

  deleteProduct(product, index) {
    const confirm = this.alertCtrl.create({
      title: 'Excluir produto',
      message: 'Deseja realmente excluir o produto selecionado?',
      buttons: [
        {
          text: 'Não'
        },
        {
          text: 'Sim',
          handler: () => {
            this._shoppingListProductsProvider.delete(this.list['id'], product['id'])
              .then((res) => {
                this.products.splice(index, 1);
                this.showToast('Produto excluído com sucesso');
              }, (err) => {
                this.flashProvider.show('Erro ao excluir o produto', 'error')
                console.log('erro ', err);
              });
          }
        }
      ]
    });
    confirm.present();
  }

  addProductManual() {
    const prompt = this.alertCtrl.create({
      title: 'Adicionar produto',
      message: "Informe o código de barras do produto",
      inputs: [
        {
          name: 'barcode',
          placeholder: 'Código de barras'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
        },
        {
          text: 'Adicionar',
          handler: data => {
            this.getProduct(data.barcode)
          }
        }
      ]
    });
    prompt.present();
  }

  async addProductScanBarcode(fab: FabContainer) {
    fab.close();

    this.optionsBarcode = {
      showFlipCameraButton: true,
      showTorchButton: true,
      disableSuccessBeep: true,
      resultDisplayDuration: 0,
      prompt: 'Posicione a câmera sobre o código de barras',
      formats: 'QR_CODE, EAN_13, EAN_8',
      orientation: "landscape"
    }

    await this.barCode.scan(this.optionsBarcode)
      .then((res) => {
        if (!res.cancelled) {
          this.getProduct(res.text);
        }
      }).catch((err) => {
        return this.flashProvider.show('Erro na leitura do Código de Barras', 'error', 3000);
      })
  }

  share() {
    const link = `http://panelideal.herokuapp.com/web/lista-compra/${this.list['id']}`
    const msg = `Oi, essa é minha lista de compra. Da uma conferida ai :) - `
    this.socialSharing.share(msg, null, null, link);
    this.updateShare();
  }

  updateShare() {
    this._shoppingListProvider.update(this.list['id'], {shared: 1})
      .then((res) => {}, (err) => {
        console.log('erro ', err);
      });
  }

  promptAlert(product) {
    let quantidade = product.quantity || '';
    if (product.quantity_purchased) {
      quantidade = product.quantity_purchased || ''
    }

    let alert = this.alertCtrl.create({
      title: 'Quantidade Comprada',
      inputs: [
        {
          name: 'quantity',
          placeholder: 'Quantidade',
          type: 'number',
          value: quantidade
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Atualizar',
          handler: data => {
            let value = lodash.replace(data.quantity, /,/g, ".");

            this._shoppingListProductsProvider.update(this.list['id'], product['id'], { quantity_purchased: value })
              .then((res) => {
                this.getProductsList();
                this.showToast('Quantidade atualizada com sucesso!')
              }, (err) => {
                return this.flashProvider.show('Não foi possivel atualizar a quantidade comprada. Tente novamente!', 'error', 3000);
              });
          }
        }
      ]
    });
    alert.present();
  }

  presentPopover(ev) {
    let popover = this.popoverCtrl.create('PopoverPage', { list: this.list });
    popover.present({ ev: ev });

    // Ação que executa após o popover fechar
    popover.onDidDismiss((res) => {
      if (res && res.status == 'deleted') {
        this.navCtrl.pop();
      } else if (res && res.status == 'updated') {
        this._shoppingListProvider.get(this.list.id)
          .then((res) => {
            this.list = res['data'];
          }).catch((err) => {
            return this.flashProvider.show('Erro ao buscar os dados atualizados da lista de compra', 'error', 3000);
          })
      }
    });
  }

  showAlert(title, message) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  showToast(message) {
    this.toast.show(message, '5000', 'bottom').subscribe(toast => { });
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Aguarde...'
    });

    this.loading.present();
  }

  async doRefresh(refresher) {
    setTimeout(async () => {
      await this.getProductsList();
      refresher.complete();
    }, 2000);
  }
}
