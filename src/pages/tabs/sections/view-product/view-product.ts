import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { ProductsProvider } from '../../../../providers/products/products';
import { FlashProvider } from '../../../../providers/shared/flash/flash';
import { LocalStorageProvider } from '../../../../providers/local-storage/local-storage';
import lodash from 'lodash';

@IonicPage()
@Component({
  selector: 'page-view-product',
  templateUrl: 'view-product.html',
})
export class ViewProductPage {
  loading: any;
  loadedProduct = false;

  segment: any;
  idProduct: any;
  product: any;
  images: any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public flashProvider: FlashProvider,
    public localStorageProvider: LocalStorageProvider,
    public _productsProvider: ProductsProvider,
    public modalCtrl: ModalController
  ) {
    this.idProduct = this.navParams.get('id');
    this.segment = 'geral';
  }

  async ionViewDidLoad() {
    this.showLoader();
    await this.getProduct();
  }

  async getProduct() {
    await this._productsProvider.get(this.idProduct)
      .then((res) => {
        this.product = res['data'];
        this.product['marca'] = lodash.filter(this.product.attributesProduct, {type: {id_unique: 'brand' }})[0]

        if (this.product.images) {
          this.product.images.forEach((element, index) => {
            this.product.images[index]['url'] = `http://54.233.242.37/image/product/${element['filename']}?format=jpg`
          });
        }
        this.images = this.product.images;
        this.loadedProduct = true;

        this.loading.dismiss();
      }, (err) => {
        console.log('erro ', err);

        this.loading.dismiss();
        this.flashProvider.show('Ocorreu um erro ao buscar os dados do produto', 'error', 3000)
        this.navCtrl.pop();
      });
  }

  openModalAddProduct(product) {
    const modal = this.modalCtrl.create(
      'ModalAddProductListPage',
      { product: product },
      { enableBackdropDismiss: true, showBackdrop: true, cssClass: 'inset-modal' }
    )
    modal.present();

    modal.onDidDismiss(data => {

    });
  }

  async doRefresh(refresher) {
    setTimeout(async () => {
      await this.getProduct();
      refresher.complete();
    }, 2000);
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Buscando o produto...'
    });

    this.loading.present();
  }
}
