import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SectionsProvider } from '../../../providers/sections/sections';
import { FlashProvider } from '../../../providers/shared/flash/flash';
import { LocalStorageProvider } from '../../../providers/local-storage/local-storage';

@IonicPage()
@Component({
  selector: 'page-sections',
  templateUrl: 'sections.html',
})
export class SectionsPage {
  loading: any;

  categories: any[] = [];
  categoriesOriginal: any[] = [];
  page = 1;
  perPage = 0;
  total = 0;
  totalPage = 0;

  searchTerm: string;
  searching: any = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public flashProvider: FlashProvider,
    public localStorageProvider: LocalStorageProvider,
    public _sectionsProvider: SectionsProvider
  ) {
  }

  ionViewDidLoad() {
    this.showLoader();
    this.getCategories(true);
  }

  async getCategories(clearCategories = false, page?: number, wBy?: string, w?: string) {
    // Id do tipo Categoria no BD é 1
    await this._sectionsProvider.getSectionsByType(1, page, wBy, w)
      .then((res) => {
        this.total = res['data']['total'];
        this.perPage = res['data']['perPage'];
        this.page = res['data']['page'];

        if (clearCategories) {
          this.categories = [];
        }

        res['data']['data'].forEach(element => {
          this.categories.push(element)
          this.categoriesOriginal.push(element)
        });

        this.localStorageProvider.set('categories', this.categories);
        this.loading.dismiss();
      }, (err) => {
        console.log('erro ', err);

        this.loading.dismiss();
        if (err.status !== 404) {
          this.flashProvider.show('Ocorreu um erro ao buscar as categorias', 'error', 3000)
        }

        this.localStorageProvider.get('categories').then(res => {
          this.categories = res ? res : [];
          this.categoriesOriginal = res ? res : [];
        });
      });
  }

  async filterItems() {
    this.searching = true;
  
    this.categories = await this.categoriesOriginal.filter((item) => {
      return item.name.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
    });

    this.searching = false;
  }

  doInfinite(infiniteScroll) {
    this.page += 1;

    setTimeout(() => {
      this.getCategories(false, this.page);
      infiniteScroll.complete();
    }, 2000);
  }

  async doRefresh(refresher) {
    setTimeout(async () => {
      await this.getCategories(true);
      refresher.complete();
    }, 2000);
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Aguarde...'
    });

    this.loading.present();
  }

  protected itemSelected(category: any) {
    this.navCtrl.push('ListProductsPage', {category: category});
  }
}
